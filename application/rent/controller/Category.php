<?php

namespace app\rent\controller;
use think\Controller;

/**
 * @author Dracowyn
 * @since 2024-01-15 17:07
 */

class Category extends Controller
{
	protected $categoryModel = null;

	public function __construct()
	{
		parent::__construct();
		$this->categoryModel = new \app\common\model\Category();
	}
	public function image()
	{
		$cateId = $this->request->param('cateid', 0, 'trim');

		$category = $this->categoryModel::find($cateId);

		if (!$category) {
			return null;
		}

		return $category['image_cdn'];
	}
}