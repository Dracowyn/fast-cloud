<?php
/**
 * @author Dracowyn
 * @since 2024-01-10 16:32
 */

namespace app\rent\controller;

use think\Controller;
use think\response\Json;

class Business extends Controller
{

	protected $businessModel = null;

	public function _initialize()
	{
		parent::_initialize();
		$this->businessModel = new \app\common\model\business\Business;
	}

	public function avatar()
	{
		if ($this->request->isPost()) {
			$busid = $this->request->param('busid', 0, 'trim');

			$business = \app\common\model\business\Business::find($busid);

			if (!$business) {
				return null;
			}

			return $business['avatar_cdn'];
		}

		return null;
	}

	public function upload(): Json
	{
		$id = $this->request->param('id', 0, 'trim');
		$business = $this->businessModel->find($id);
		if (!$business) {
			return json(['msg' => '用户不存在', 'code' => 0, 'data' => null]);
		}
		$avatar = build_upload('avatar');
		return json($avatar);
	}

}