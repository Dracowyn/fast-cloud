<?php

namespace app\common\model;

use think\db\exception\DataNotFoundException;
use think\db\exception\ModelNotFoundException;
use think\Env;
use think\exception\DbException;
use think\Model;

/**
 * 分类模型
 */
class Category extends Model
{

	// 开启自动写入时间戳字段
	protected $autoWriteTimestamp = 'int';
	// 定义时间戳字段名
	protected $createTime = 'createtime';
	protected $updateTime = 'updatetime';
	// 追加属性
	protected $append = [
		'type_text',
		'flag_text',
		'image_cdn',
	];

	protected static function init()
	{
		self::afterInsert(function ($row) {
			$row->save(['weigh' => $row['id']]);
		});
	}

	public function setFlagAttr($value, $data)
	{
		return is_array($value) ? implode(',', $value) : $value;
	}

	/**
	 * 读取分类类型
	 * @return array
	 */
	public static function getTypeList()
	{
		$typeList = config('site.categorytype');
		foreach ($typeList as $k => &$v) {
			$v = __($v);
		}
		return $typeList;
	}

	public function getTypeTextAttr($value, $data)
	{
		$value = $value ? $value : $data['type'];
		$list = $this->getTypeList();
		return $list[$value] ?? '';
	}

	public function getFlagList()
	{
		return ['hot' => __('Hot'), 'index' => __('Index'), 'recommend' => __('Recommend')];
	}

	public function getFlagTextAttr($value, $data)
	{
		$value = $value ? $value : $data['flag'];
		$valueArr = explode(',', $value);
		$list = $this->getFlagList();
		return implode(',', array_intersect_key($list, array_flip($valueArr)));
	}

	/**
	 * 读取分类列表
	 * @param string|null $type 指定类型
	 * @param string|null $status 指定状态
	 * @return array
	 * @throws DataNotFoundException
	 * @throws ModelNotFoundException
	 * @throws DbException
	 */
	public static function getCategoryArray(string $type = null, string $status = null): array
	{
		return collection((array)self::where(function ($query) use ($type, $status) {
			if (!is_null($type)) {
				$query->where('type', '=', $type);
			}
			if (!is_null($status)) {
				$query->where('status', '=', $status);
			}
		})->order('weigh', 'desc')->select())->toArray();
	}

	public function getImageCdnAttr($value, $data)
	{
		$cdn = Env::get('site.cdn_url', config('site.cdn_url')) ?? Env::get('site.url', config('site.url'));
		return $cdn . $data['image'];
	}
}
